local L = LibStub( "AceLocale-3.0" ):NewLocale( "TitanClan", "ruRU" );
if ( not L ) then return end

L["TITAN_CLAM_MENU_CLAN"] = "Клан"
L["TITAN_CLAN_ABOUT"] = "Об аддоне"
L["TITAN_CLAN_ABOUT_AUTHOR"] = "|cffffd700Автор :|r |cffff8c00 %s|r"
L["TITAN_CLAN_ABOUT_CATEGORY"] = "|cffffd700Категория :|r |cffff8c00 %s|r"
L["TITAN_CLAN_ABOUT_DESC"] = "Аддон для Титан Панели. Этот аддон отображает на Титан Панели ваших друзей, отсортированных по типу подключения."
L["TITAN_CLAN_ABOUT_EMAIL"] = "|cffffd700Email :|r |cffff8c00 %s|r"
L["TITAN_CLAN_ABOUT_MEMORY"] = "|cffffd700Использование памяти :|r |cffffffff%skb|r"
L["TITAN_CLAN_ABOUT_TRANSLATION"] = "|cffffd700Переводы :|r |cffff8c00 %s|r"
L["TITAN_CLAN_ABOUT_VERSION"] = "|cffffd700Версия :|r |cff20ff20 r%s|r"
L["TITAN_CLAN_ABOUT_WEBSITE"] = "|cffffd700Сайт :|r |cff00ccff%s|r"
L["TITAN_CLAN_ADDON_LABEL"] = "Titan [|cffeda55fClan|r]"
L["TITAN_CLAN_ARRAY"] = "|cffeda55f%s|r исправлено!"
L["TITAN_CLAN_ARRAY_FIXED"] = "TitanClan.|cffeda55f%s|r исправлен!"
L["TITAN_CLAN_BUTTON_CLANLESS"] = "Без гильдии"
L["TITAN_CLAN_BUTTON_LABEL"] = "%s :"
L["TITAN_CLAN_BUTTON_TEXT"] = "%s/%s"
L["TITAN_CLAN_CONFIG_ASCENDING"] = "Ascending" -- Requires localization
L["TITAN_CLAN_CONFIG_AWAY"] = "Отошёл"
L["TITAN_CLAN_CONFIG_BANNER"] = "Отображать Load Banner"
L["TITAN_CLAN_CONFIG_BANNER_DESC"] = "Опция для отображения информации об аддоне и использовании им памяти при загрузке в стандартном чате"
L["TITAN_CLAN_CONFIG_BANNER_FORMAT"] = "%s |cff00ff00r%s|r LЗагружено, используется |cff00ff00%s|r памяти."
L["TITAN_CLAN_CONFIG_BUSY"] = "Занят"
L["TITAN_CLAN_CONFIG_CLASS"] = "Класс"
L["TITAN_CLAN_CONFIG_CUSTOM_TOOLTIP"] = "Добавить новый формат подсказок"
L["TITAN_CLAN_CONFIG_CUSTOM_TOOLTIP_DESC"] = "Здесь вы можете добавить новый формат подсказок"
L["TITAN_CLAN_CONFIG_CUSTOM_TOOLTIP_INST"] = [=[
|cff20ff20Вы можете использовать :|r
     |cff20ff20!p|r для Игрока
     |cff20ff20!c|r для Класса
     |cff20ff20!l|r для Уровня
     |cff20ff20!r|r для Ранга
     |cff20ff20!s|r для Статуса
     |cff20ff20!n|r для Заметки
     |cff20ff20!on|r для Офицерской заметки
     |cff20ff20!z|r для Зоны
     |cff20ff20!uc|r для Цветного Имени Игрока

 |cffff9900Правая сторона должна быть отделена от левой знаком  ~ (тильда).|r]=]
L["TITAN_CLAN_CONFIG_DESCENDING"] = "Descending" -- Requires localization
L["TITAN_CLAN_CONFIG_HEADER_ADD"] = "Новый формат"
L["TITAN_CLAN_CONFIG_HEADER_REMOVE"] = "Удалить Формат"
L["TITAN_CLAN_CONFIG_HEADER_SETTINGS"] = "Настройки"
L["TITAN_CLAN_CONFIG_HEADER_TOOLTIP"] = "Формат подсказок"
L["TITAN_CLAN_CONFIG_LABEL"] = "Аддон отображает статус-лист членов вашей гильдии на кнопке Титан Панели"
L["TITAN_CLAN_CONFIG_LEVEL"] = "Уровень"
L["TITAN_CLAN_CONFIG_MAXTOOLTIP"] = "Подсказка Макс"
L["TITAN_CLAN_CONFIG_MAXTOOLTIP_DESC"] = "Максимальное число членов клана для отображения за раз на всплывающей подсказке"
L["TITAN_CLAN_CONFIG_NAME"] = "Имя"
L["TITAN_CLAN_CONFIG_NONE"] = "Никого"
L["TITAN_CLAN_CONFIG_PRESETS"] = "Преднастройки всплывающей подсказки"
L["TITAN_CLAN_CONFIG_PRESETS_DESC"] = "Немного разных преднастроенные форматов для отображения на всплывающей подсказке"
L["TITAN_CLAN_CONFIG_RANK"] = "Ранг"
L["TITAN_CLAN_CONFIG_REMOVE"] = "Удалить"
L["TITAN_CLAN_CONFIG_REMOVE_DESC"] = "Удаляет выбранный формат из преднастроенных"
L["TITAN_CLAN_CONFIG_REMOVE_PRESETS_DESC"] = "Пожалуйста выберите формат для удаления!"
L["TITAN_CLAN_CONFIG_SHOWMEM"] = "Показать использование памяти"
L["TITAN_CLAN_CONFIG_SHOWMEM_DESC"] = "Отображает количество памяти, используемое %s на Всплывающей подсказке."
L["TITAN_CLAN_CONFIG_SORTMETHOD"] = "Sort Method :" -- Requires localization
L["TITAN_CLAN_CONFIG_SORTMETHOD_DESC"] = "The method in which the tooltip will be sorted." -- Requires localization
L["TITAN_CLAN_CONFIG_SORTTYPES"] = "Настройки сортировки :"
L["TITAN_CLAN_CONFIG_SORTTYPES_DESC"] = "Разные варианты сортировки подсказки"
L["TITAN_CLAN_CONFIG_UNKNOWN"] = "Неизвестно"
L["TITAN_CLAN_CONFIG_ZONE"] = "Зона"
L["TITAN_CLAN_CORE_ADDAUTHOR"] = "|cff00ff00%s|r Добавлен как Автор"
L["TITAN_CLAN_CORE_DEBUG"] = "Отладка"
L["TITAN_CLAN_CORE_DEBUG_DESC"] = "Разрешить режим Отладки"
L["TITAN_CLAN_CORE_DEBUG_EVENT"] = "События"
L["TITAN_CLAN_CORE_DEBUG_EVENT_DESC"] = "Отображает события в окне чата в тот момент, когда они происходят"
L["TITAN_CLAN_CORE_DEBUG_HEADER"] = "Отладка"
L["TITAN_CLAN_CORE_DEBUG_LOGEVENT"] = "Лог Событий"
L["TITAN_CLAN_CORE_DEBUG_LOGEVENT_DESC"] = "Записывает события для дальнейшего просмотра"
L["TITAN_CLAN_CORE_DEBUG_REMAUTH"] = "Удалить Автора"
L["TITAN_CLAN_CORE_DEBUG_REMAUTH_DESC"] = "Удаляет |cffff0000%s|r как Автора Аддона"
L["TITAN_CLAN_CORE_REMAUTHOR"] = "|cffff0000%s|r Удалён как Автор"
L["TITAN_CLAN_DEBUG_CMDE"] = "|cffff9900<TitanClanDebug>|r Извините, но |cfff660ab%s|r это не верная команда! В файле |cfff660ab%s|r строка |cfff660ab#%d|r."
L["TITAN_CLAN_GUILD_NOTIFY_CONCENT"] = [=[Titan [|cffeda55fClan|r] использует "|cffff0000Оповещение члена гильдии|r" для обновления подсказки.

 Функция сейчас |cffff0000выключена|r Включить её |cff00ff00для вас|r?

 |cffc0c0c0( Отмена запретит обновление подсказки )|r]=]
L["TITAN_CLAN_HELP_ADDAUTH"] = "  |cffff0000/%s AddAuth|r - Добавить Автора Аддона в БД"
L["TITAN_CLAN_HELP_CMD_UNKNOWN"] = "Неверная команда!"
L["TITAN_CLAN_HELP_ERROR"] = "Извините, но |cffff0000%s|r это неверная команда!"
L["TITAN_CLAN_HELP_LINE1"] = "Использование:"
L["TITAN_CLAN_HELP_LINE2"] = "  |cff00ff00/%s help|r - Показывает это меню"
L["TITAN_CLAN_HELP_LINE3"] = "  |cff00ff00/%s [ options | config ]|r - Открывает панель опций"
L["TITAN_CLAN_HELP_LINE4"] = "  |cff00ff00/%s reset|r - Сбрасывает аддон к настройкам по-умолчанию и перезагружает интерфейс"
L["TITAN_CLAN_HELP_LINE5"] = "  |cff00ff00/%s checkDB|r - Проверяет БД на ошибки"
L["TITAN_CLAN_HELP_REMAUTH"] = "  |cffff0000/%s RemAuth|r - Удалить Автора Аддона из БД"
L["TITAN_CLAN_LOCALSARRAY_FIXED"] = "TitanClanLocals.|cffeda55f%s|r исправлено!"
L["TITAN_CLAN_MENU_ADDFRIEND"] = "Добавить %s к друзьям."
L["TITAN_CLAN_MENU_CHECKDB"] = "Проверить БД на ошибки"
L["TITAN_CLAN_MENU_CHECKDB_FINSIHED"] = "Проверка БД завершена!"
L["TITAN_CLAN_MENU_CLOSE"] = "Закрыть Меню"
L["TITAN_CLAN_MENU_INVITE"] = "Пригласить %s в группу."
L["TITAN_CLAN_MENU_SETTINGS"] = "Настройки"
L["TITAN_CLAN_MENU_TEXT"] = "Titan Clan"
L["TITAN_CLAN_MENU_WHISPER"] = "Шепнуть %s."
L["TITAN_CLAN_MENU_WHO"] = "%s кто?"
L["TITAN_CLAN_STATICARRAY_FIXED"] = "TitanClanStatisArray.|cffeda55f%s|r исправлено!"
L["TITAN_CLAN_TOOLTIP"] = "Информация о Клане"
L["TITAN_CLAN_TOOLTIP_CLANLESS"] = "  Вы без Клана"
L["TITAN_CLAN_TOOLTIP_GUILD_FORMAT"] = "<%s>"
L["TITAN_CLAN_TOOLTIP_LEVEL_TEXT"] = "Уровень"
L["TITAN_CLAN_TOOLTIP_MEM"] = "Использование Памяти"
