## Interface: 10002
## Title: Titan Panel [|cffeda55fClan|r] |cff00aa005.23.7.90207|r
## Notes: Guild Managment Plugin for Titan Panel
## Authors: KanadiaN
## SavedVariables: TitanClan, TitanClanInit, TitanClanStaticArrays, TitanClanLocals, TitanClanDebug, TitanClanProfile
## SavedVariablesPerCharacter: TitanClanGuild
## OptionalDeps: Addon Control Panel
## Dependencies: Titan
## Version: 45
## X-Child-of: Titan
## X-Compatible-With: 100002
## X-Translation:
## X-Category: Interface Enhancements
## X-Website: http://wow.curse.com/addons/wow/titan-panel-clan
## X-Email: bug@kanadian.net
## X-License: GNU General Public License version 3 (GPLv3) 

Locals\Locals.xml

TitanClanConfig.lua
TitanClan.lua
TitanClanToolTip.lua
TitanClanRightClick.lua
TitanClanUtils.lua

TitanClan.xml
