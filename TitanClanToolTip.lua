-- **************************************************************************
--   TitanClanToolTip.lua
--
--   By: KanadiaN
--        (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanClan", true );

-- **************************************************************************
-- NAME : TitanPanelClanButton_SetTooltipText()
-- DESC : Build Firends Tooltip for Titan Panel
-- **************************************************************************
function TitanPanelClan_SetTooltip()
     GameTooltip:AddLine( LB["TITAN_CLAN_TOOLTIP"], HIGHLIGHT_FONT_COLOR.r, HIGHLIGHT_FONT_COLOR.g, HIGHLIGHT_FONT_COLOR.b );
     if ( IsInGuild() ) then
          TitanClan_GuildUtils( "gSort" );
          local ttleft, ttright, name, rank, rIndex, level, class, zone, note, oNote, online, status, classFileName, cGuild, cLevel, memStr, gInfo, key, keydata, ttCount, gLevel;
          local gData = TitanClanGuild;
          cGuild = TitanClan_ColorUtils( "cRank", TitanClanProfile.rankIndex, LB["TITAN_CLAN_TOOLTIP_GUILD_FORMAT"]:format( gData.Name ), gData.numRanks );
          lText = TitanClan_ColorUtils( "cText", "white", LB["TITAN_CLAN_TOOLTIP_LEVEL_TEXT"] );
          GameTooltip:AddLine( cGuild );
          ttCount = 1;
          table.foreach( gData.Stream, function( Key, Data )
                    if ( ttCount <= TitanClan.Settings.maxTooltip ) then
                         ttleft, ttright = TitanClan_CoreUtils( "Tooltip", TitanClan.Settings.tFormat, Data.Name, Data.Index, Data.Rank, gData.numRanks, Data.Level, Data.Class, Data.Zone, Data.Status, Data.Note, Data.oNote );
                         GameTooltip:AddDoubleLine( ttleft, ttright );
                         ttCount = ttCount + 1;
                    elseif ( ttCount >= TitanClan.Settings.maxTooltip ) then
                         TitanClan_DebugUtils( "Bug", "SetTooltip", 34, "Max Tooltip Error" );
                    end
               end
          );
     else
          GameTooltip:AddLine( LB["TITAN_CLAN_TOOLTIP_CLANLESS"] );
     end
     -- For showing AddOn memory Usage
     if ( TitanClan.Settings.ShowMem ) then
          GameTooltip:AddLine( "\n" );
          GameTooltip:AddLine( TitanClan_ColorUtils( "cText", "white", LB["TITAN_CLAN_TOOLTIP_MEM"] ) );
	  GameTooltip:AddDoubleLine( ( "  %s"):format( LB["TITAN_CLAN_ADDON_LABEL"] ), TitanClan_ColorUtils( "cText", "green", ( "%s kb" ):format( floor( GetAddOnMemoryUsage( "TitanClan" ) ) ) ) )
     end
end
