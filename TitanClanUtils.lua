-- **************************************************************************
--   TitanClanUtils.lua
--
--   By: KanadiaN
--        (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanClan", true );

-- **************************************************************************
-- NAME : TitanClan_CoreUtils( b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12 )
-- DESC : This makes everything happen
-- **************************************************************************
function TitanClan_CoreUtils( b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12 )
     local b = { [1] = b1, [2] = b2, [3] = b3, [4] = b4, [5] = b5, [6] = b6, [7] = b7, [8] = b8, [9] = b9, [10] = b10, [11] = b11, [12] = b12, };
     if ( TitanClanDebug.State and TitanClanDebug.Event ) then
          local tdLine
          for i = 1, 10 do
               if ( i == 1 ) then
                    if ( b[i] ) then tdLine = ( "[|cffff9900%s|r]" ):format( b[i] ); end
               else
                    if ( b[i] ) then tdLine = tdLine .. ( "[|cffff9900%s|r]" ):format( b[i] ); end
               end
          end
          TitanClanDebug.dLine = ( "Recieved : %s" ):format( tdLine );
	  TitanClan_DebugUtils( "Bug", "TitanClan_CoreUtils", 27, TitanClanDebug.dLine );
     end
     if ( b[1] == "Banner" ) then
          --[b1    ]
          --[Action]
          UpdateAddOnMemoryUsage();
          TitanClanDebug.dLine = ( "%s kb" ):format( floor( GetAddOnMemoryUsage( "TitanClan" ) ) );
          TitanClan_DebugUtils( "Info", "TitanClanUtils", 35, LB["TITAN_CLAN_CONFIG_BANNER_FORMAT"]:format( LB["TITAN_CLAN_ADDON_LABEL"], LB["TITAN_CLAN_CORE_VERSION"], TitanClanDebug.dLine ) );
          TitanClanDebug.dLine = "";
     elseif ( b[1] == "Safe" ) then
          local text = b[2];
          if ( text == true ) then text = "true";
          elseif ( text == false ) then text = "false";
          elseif ( text == nil ) then text = "nil"; end
          return text;
     elseif ( b[1] == "Tooltip" ) then
          --[b1    ][b2    ][b3    ][b4   ][b5  ][b6  ][b7   ][b8   ][b9  ][b10   ][b11 ][b12        ]
          --[Action][Format][Player][index][rank][nums][Level][Class][Zone][Status][Note][OfficerNote]
          --[[ [ !s - Status ][ !p - Player ][ !r - Rank ][ !l - Level ][ !c - Class ][ !z - Zone ][ !uc - Colored Players Name ] ]]--
          b[2] = string.gsub( b[2], "!uc", TitanClan_ColorUtils( "cClass", b[8], b[3] ) );
          b[2] = string.gsub( b[2], "!p", TitanClan_ColorUtils( "cText", "yellow", b[3] ) );
          b[2] = string.gsub( b[2], "!r", TitanClan_ColorUtils( "cRank", b[4], b[5], b[6] ) );
          b[2] = string.gsub( b[2], "!l", TitanClan_ColorUtils( "cText", "orange", b[7] ) );
          b[2] = string.gsub( b[2], "!c", TitanClan_ColorUtils( "cClass", b[8], b[8] ) );
          b[2] = string.gsub( b[2], "!n", TitanClan_ColorUtils( "cText", "gray", b[11] ) );
          b[2] = string.gsub( b[2], "!on", TitanClan_ColorUtils( "cText", "white", b[12] ) );
          b[2] = string.gsub( b[2], "!z", TitanClan_ColorUtils( "cText", "sgray", b[9] ) );
          if ( b[10] == 1 ) then
               b[2] = string.gsub( b[2], "!s", ( "  %s" ):format( TitanClan_ColorUtils( "cText", "sgray", TitanClan.Settings.sFormat ) ) );
          elseif ( b[10] == 2 ) then
               b[2] = string.gsub( b[2], "!s", ( "  %s" ):format( TitanClan_ColorUtils( "cText", "sgray", TitanClan.Settings.dFormat ) ) );
          else
               b[2] = string.gsub( b[2], "!s", "  " );
          end
          b[11], b[12] = string.match( b[2], "(.*)~(.*)" );
          return b[11], b[12];
     elseif ( b[1] == "Author" ) then
           local Authors = {
               ["Lindarena@Laughing Skull"] = true,
               ["Stâbsâlot@Laughing Skull"] = true,
               ["Hilyna@Laughing Skull"] = true,
               ["Nagdand@Laughing Skull"] = true,
               ["Holycurves@Laughing Skull"] = true,
               ["Holytree@Laughing Skull"] = true,
               ["Acatra@Laughing Skull"] = true,
               ["Nâsh@Laughing Skull"] = true,
               ["Loansone@Laughing Skull"] = true,
               ["Koton@Laughing Skull"] = true,
               ["Wenling@Laughing Skull"] = true,
          }
          for i = 1, getn( TitanClan.AddAuth ) do
               if ( not Authors[TitanClan.AddAuth[i]] ) then
                    Authors[TitanClan.AddAuth[i]] = true;
               else
                    tremove( TitanClan.AddAuth, i );
                    TitanClan_DebugUtils( "Bug", "TitanClanUtils", 81, "Can Not Add Author Already Exists!" )
               end
          end
          return Authors[b[2]];
     elseif ( b[1] == "AddAuth" or b[1] == "addauth" ) then
          b[2] = ( "%s@%s" ):format( TitanClanProfile.Player, TitanClanProfile.Realm );
          TitanClan_DebugUtils( "Info", "TitanClanUtils", 87, LB["TITAN_CLAN_CORE_ADDAUTHOR"]:format( b[2] ) );
          tinsert( TitanClan.AddAuth, b[2] );
          
     elseif ( b[1] == "RemAuth" or b[1] == "remauth" ) then
          b[2] = ( "%s@%s" ):format( TitanClanProfile.Player, TitanClanProfile.Realm );
          for i = 1, getn( TitanClan.AddAuth ) do
               if ( b[2] == TitanClan.AddAuth[i] ) then
                    TitanClan_DebugUtils( "Info", "TitanClanUtils", 94, LB["TITAN_CLAN_CORE_REMAUTHOR"]:format( b[2] ) );
                    tremove( TitanClan.AddAuth, i );
               end
          end
     elseif ( b[1] == "getLocals" ) then
          local localClass = {};
          local Sex, Gender, gType
          table.foreach( { { true, "TRUE", }, { false, "FALSE", }, }, function( key, data )
                    table.foreach( FillLocalizedClassList( localClass, data[1] ), function( English, Local )
                              -- local color = RAID_CLASS_COLORS[English];
                              local color = C_ClassColor.GetClassColor( English );
                              local colorCode = ( "%02x%02x%02x" ):format( ( color.r * 255 ), ( color.g * 255 ), ( color.b * 255 ) );
                              local richText = ( "|cff%s%%s|r" ):format( colorCode );
                              TitanClan_DebugUtils( "Bug", "getLocals", 143, richText:format( "test" ) );
                              Gender = ( "%s_%s" ):format( English, data[2] );
                              TitanClanLocals.ColorClass[Gender] = richText;
                              TitanClanLocals.LocalClass[Gender] = Local;
                              TitanClanLocals.ToEnglish[Local] = Gender;
                         end
                    )
               end
          )
          TitanClanProfile.Locale = GetLocale();
     elseif ( b[1] == "tAmbiguate"  ) then
          TitanClan.Settings.Ambiguate = not TitanClan.Settings.Ambiguate;
     elseif ( b[1] == "Ambiguate"  ) then
          local name, tRealm, len1, len2, len3
          if ( TitanClan.Settings.Ambiguate == false ) then
               tRealm = ( "-%s" ):format( gsub( TitanClanProfile.Realm, " ", "" ) );
               len1 = strlen( tRealm );
               len2 = strlen( b[2] )
               len3 = floor( len2 - len1 );
               name = strsub( b[2], 1, len3 );
          elseif ( TitanClan.Settings.Ambiguate == true ) then
               name = Ambiguate( b[2], TitanClan.Settings.aStyle );
          end
	  return name;
     elseif ( b[1] == "setIcon" ) then
          TitanClan.Settings.iCount = TitanClan.Settings.iCount + 1;
          local icon = TitanPanelClanButtonIcon
          SetSmallGuildTabardTextures( "player", icon, GuildFrameTabardBackground, GuildFrameTabardBorder ); 
     end
end

-- **************************************************************************
-- NAME : TitanClan_GuildUtils( g1, g2, g3, g4, g5 )
-- DESC : Functions for handeling the guild info
-- **************************************************************************
function TitanClan_GuildUtils( g1, g2, g3, g4, g5 )
     local g = { [1] = g1, [2] = g2, [3] = g3, [4] = g4, [5] = g5, };
     if ( g[1] == "gSetup" ) then 
          if ( IsInGuild() ) then
               if ( not TitanClanGuild or TitanClanGuild == nil ) then TitanClanGuild = {}; end
               if ( not TitanClanGuild.Ranks or TitanClanGuild.Ranks == nil ) then TitanClanGuild.Ranks = {}; end
               if ( not TitanClanGuild.Stream or TitanClanGuild.Stream == nil ) then TitanClanGuild.Stream = {}; end
               if ( not TitanClanGuild.Total or TitanClanGuild.Total == nil ) then TitanClanGuild.Total = 0; end
               if ( not TitanClanGuild.TotalOnline or TitanClanGuild.TotalOnline == nil ) then TitanClanGuild.TotalOnline = 0; end
               if ( not TitanClanGuild.numRanks or TitanClanGuild.numRanks == nil ) then TitanClanGuild.numRanks = 0; end
               if ( not TitanClanGuild.Time or TitanClanGuild.Time == nil ) then TitanClanGuild.Time = 0; end
          end
     elseif ( g[1] == "buildRanks" ) then
          -- For building Ranks
          local curCount = GuildControlGetNumRanks();
          TitanClanGuild.Name = GetGuildInfo( "player" );
          TitanClanGuild.Ranks = {};
          for index = 1, curCount do
               local rankName = GuildControlGetRankName( index );
               table.insert( TitanClanGuild.Ranks, index, rankName );
          end
          TitanClanGuild.numRanks = curCount;
     elseif ( g[1] == "buildGuild" ) then
          -- For build online guild members list
          local curTotal, curTotalOnline = GetNumGuildMembers();
          local curTime = floor( GetTime() );
          local checkTime = floor( curTime - TitanClan.Settings.oldTime );
          if ( checkTime > 10 or TitanClan.Settings.oldTime == 0 ) then
               TitanClanGuild.oldTime = curTime
               TitanClanGuild.Stream = {};
               for gIndex = 1, curTotal do
                    local name, rankName, rankIndex, level, class, zone, note, oNote, online, status, classFileName = GetGuildRosterInfo( gIndex );
                    if ( online ) then
                         name = TitanClan_CoreUtils( "Ambiguate", name );
                         if ( name == UnitName( "player" ) ) then TitanClanProfile.rankIndex = rankIndex; end
                         table.insert( TitanClanGuild.Stream, { Name = name, Rank = rankName, Index = rankIndex, Level = level, Class = class, Zone = zone, Status = status, Note = note, oNote = oNote, gNum = gIndex, } ); --Ambiguate( name, "guild" )
                    end
               end
               TitanClanGuild.Total = curTotal;
               TitanClanGuild.TotalOnline = curTotalOnline;

          end
     elseif ( g[1] == "gSort" ) then
          local sortBy = TitanClan.Settings.sType;
          local sortMethod = TitanClan.Settings.sMethod;
          if ( sortBy == LB["TITAN_CLAN_CONFIG_NAME"] ) then
               if ( sortMethod == LB["TITAN_CLAN_CONFIG_ASCENDING"] ) then
                    table.sort( TitanClanGuild.Stream, function ( key1, key2 ) return key1.Name < key2.Name end ); 
               elseif ( sortMethod == LB["TITAN_CLAN_CONFIG_DESCENDING"] ) then
                    table.sort( TitanClanGuild.Stream, function ( key1, key2 ) return key1.Name > key2.Name end ); 
               end
          elseif ( sortBy == LB["TITAN_CLAN_CONFIG_RANK"] ) then
               if ( sortMethod == LB["TITAN_CLAN_CONFIG_ASCENDING"] ) then
                    table.sort( TitanClanGuild.Stream, function ( key1, key2 ) return key1.Index < key2.Index end );
               elseif ( sortMethod == LB["TITAN_CLAN_CONFIG_DESCENDING"] ) then
                    table.sort( TitanClanGuild.Stream, function ( key1, key2 ) return key1.Index > key2.Index end ); 
               end
          elseif ( sortBy == LB["TITAN_CLAN_CONFIG_LEVEL"] ) then
               if ( sortMethod == LB["TITAN_CLAN_CONFIG_ASCENDING"] ) then
                    table.sort( TitanClanGuild.Stream, function ( key1, key2 ) return key1.Level < key2.Level end ); 
               elseif ( sortMethod == LB["TITAN_CLAN_CONFIG_DESCENDING"] ) then
                    table.sort( TitanClanGuild.Stream, function ( key1, key2 ) return key1.Level > key2.Level end ); 
               end
          elseif ( sortBy == LB["TITAN_CLAN_CONFIG_CLASS"] ) then
               if ( sortMethod == LB["TITAN_CLAN_CONFIG_ASCENDING"] ) then
                    table.sort( TitanClanGuild.Stream, function ( key1, key2 ) return key1.Class < key2.Class end ); 
               elseif ( sortMethod == LB["TITAN_CLAN_CONFIG_DESCENDING"] ) then
                    table.sort( TitanClanGuild.Stream, function ( key1, key2 ) return key1.Class > key2.Class end ); 
               end
          elseif ( sortBy == LB["TITAN_CLAN_CONFIG_ZONE"] ) then
               if ( sortMethod == LB["TITAN_CLAN_CONFIG_ASCENDING"] ) then
                    table.sort( TitanClanGuild.Stream, function ( key1, key2 ) return key1.Zone < key2.Zone end ); 
               elseif ( sortMethod == LB["TITAN_CLAN_CONFIG_DESCENDING"] ) then
                    table.sort( TitanClanGuild.Stream, function ( key1, key2 ) return key1.Zone > key2.Zone end ); 
               end
          end
     end
end

-- **************************************************************************
-- NAME : TitanClan_ResetUtils( r1, r2, r3, r4, r5 )
-- DESC : Diffrent ways to fix / reset Data Array
-- **************************************************************************
function TitanClan_ResetUtils( r1, r2, r3, r4, r5 )
     local r = { [1] = r1, [2] = r2, [3] = r3, [4] = r4, [5] = r5, };
     --[r1    ][r2    ][r3  ][r4   ]
     --[reload][action][pram][value]
     if ( r[2] == "reset" ) then
          if ( r[3] == "settings" ) then
               TitanClanInit = nil;
               TitanClan.Ssettings = nil;
          elseif ( r[3] == "locals" and r[4] == "LocalClass" or r[4] == "ToEnglish" or r[4] == "ColorClass" ) then
               TitanClanLocals[r[4]] = {};
               TitanClan_CoreUtils( "getLocals" );
          elseif ( r[3] == "array" and r[4] == "presets" ) then
               TitanClanInit = false;
               TitanclanStaticArrays[r[4]] = nil;
               r[1] = true;
          elseif ( r[3] == "all" ) then
               TitanClan_DebugUtils( "Info", "ResetUtils", 223, "Recieved All Command" )
               if ( r[4] ) then
                    if ( r[4] == "locals" ) then 
                         TitanClanLocals = {};
                         TitanClanLocals.LocalClass = {};
                         TitanClanLocals.ColorClass = {};
                         TitanClanLocals.ToEnglish = {};
                         TitanClan_CoreUtils( "getLocals" );
                         r[1] = false;
                    elseif ( r[4] == "array" ) then
                         TitanClanInit = false;
                         TitanClanStaticArrays = nil;
                         r[1] = true;
                    end
               elseif ( not r[4] ) then
                    TitanClanInit = false;
                    TitanClan = nil;
                    TitanClanStaticArrays = nil;
                    TitanClanLocals = nil;
                    TitanClanDebug = nil;
                    TitanClanProfile = nil;
                    TitanClanGuild = nil;
               end
          else 
               TitanClan_DebugUtils( "Error", "TitanClanUtils", 246, LB["TITAN_CLAN_HELP_CMD_UNKNOWN"] );
          end
     elseif ( r[2] == "checkdb" ) then
          TitanClan_Init( "checkDB" );
          TitanPanelButton_UpdateButton( LB["TITAN_CLAN_CORE_ID"] );
     else 
          TitanClan_DebugUtils( "Error", "TitanClanUtils", 252, LB["TITAN_CLAN_HELP_CMD_UNKNOWN"] );
     end
     if ( r[1] ) then ReloadUI(); end
end

-- **************************************************************************
-- NAME : TitanClan_ColorUtils( c1, c2, c3, c4, c5, c6 )
-- DESC : Text coloring function
-- **************************************************************************
function TitanClan_ColorUtils( c1, c2, c3, c4, c5, c6 )
     local c = { [1] = c1, [2] = c2, [3] = c3, [4] = c4, [5] = c5, [6] = c6, };
     if ( c[1] == "cText" ) then
          --[c1    ][c2   ][c3  ]
          --[Action][Color][Text]
          local color = strupper( c[2] )
          local colorCache = {
               GREEN = "|cff00ff00%s|r",
               BLUE = "|cff00ffff%s|r",
               GRAY = "|cff808080%s|r",
               SGRAY = "|cff98afc7%s|r",
               WHITE = "|cffffffff%s|r",
               YELLOW = "|cffffd200%s|r",
               NORMAL = "|cffffd200%s|r",
               ORANGE = "|cffff9900%s|r",
               PINK = "|cfff660ab%s|r",
               RED = "|cffff0000%s|r",
          }
          return colorCache[color]:format( c[3] );
     elseif ( c[1] == "cClass" ) then
          --[c1    ][c2   ][c3    ]
          --[Action][Class][Player]
          local classEng, localClass, colorCode;
          classEng = TitanClanLocals.ToEnglish[c[2]];
          localClass = TitanClanLocals.LocalClass[classEng];
          colorCode = TitanClanLocals.ColorClass[classEng];
          return colorCode:format( c[3] );
     -- START NOTE :
     -- =============================================================================
     -- This code is from Titan Guild, Credits go to the author that created it
     -- Old Author: chicogrande (jluzier@gmail.com)
     -- Old Version: 3.53
     -- Current Author: SeGeDel
     -- Current Version: 3.6l
     -- It has been edited to work with TitanClan.
     -- =============================================================================
     elseif ( c[1] == "cRank" ) then
          --[c1    ][c2   ][c3  ][c4  ]
          --[Action][index][rank][nums]
          local colorCode, pct, pctmod, r, g, b, color;
          color = {
               ["Green"] = { ["r"] = 0.1, ["g"] = 1, ["b"] = 0.1, }, -- [1]
               ["Yellow"] = { ["r"] = 1, ["g"] = 0.82, ["b"] = 0, }, -- [2]
               ["Red"] = { ["r"] = 1, ["g"] = 0.1, ["b"] = 0.1, }, -- [3]
               ["r"] = 0,
               ["g"] = 0,
               ["b"] = 0,
          };
          pct = ( ( c[2] * 100 ) / c[4] ) / 100;
          if ( c[2] == 0 ) then
               color = color.Red;
          elseif ( c[2] == ( c[4] / 2 ) ) then
               color = color.Yellow;
          elseif ( c[2] == c[4] ) then
               color = color.Green;
          elseif ( c[2] > ( c[4] / 2 ) ) then
               pctmod = ( 1.0 - pct ) * 2;
               color.r = ( color.Yellow.r - color.Green.r ) * pctmod + color.Green.r;
               color.g = ( color.Yellow.g - color.Green.g ) * pctmod + color.Green.g;
               color.b = ( color.Yellow.b - color.Green.b ) * pctmod + color.Green.b;
          elseif ( c[2] < ( c[4] / 2 ) ) then
               pctmod = ( 0.5 - pct ) * 2;
               color.r = ( color.Red.r - color.Yellow.r ) * pctmod + color.Yellow.r;
               color.g = ( color.Red.g - color.Yellow.g ) * pctmod + color.Yellow.g;
               color.b = ( color.Red.b - color.Yellow.b ) * pctmod + color.Yellow.b;
          end
          r = ( "%02x" ):format( color.r * 255 ); g = ( "%02x" ):format( color.g * 255 ); b = ( "%02x" ):format( color.b * 255 );
          colorCode = ( "|cff%s%s%s%s|r" ):format( r, g, b, c[3] );
          return colorCode;
     end
     -- =============================================================================
     -- END NOTE :
end

-- **************************************************************************
-- NAME : TitanClan_DebugUtils( d, d1, d2, d3, d4, d5 )
-- DESC : For debug messaging and print messages to chat frame
-- **************************************************************************
function TitanClan_DebugUtils( d1, d2, d3, d4, d5 )
     local d = { [1] = d1, [2] = d2, [3] = d3, [4] = d4, [5] = d5, };
     --[d1    ][d2      ][d3  ][d4     ]
     --[Action][Function][Line][Message]
     if ( d[1] == "Info" ) then
	  DEFAULT_CHAT_FRAME:AddMessage( LB["TITAN_CLAN_CORE_ITAG"]:format( d[4] ) );
     elseif ( d[1] == "Error" ) then
          DEFAULT_CHAT_FRAME:AddMessage( LB["TITAN_CLAN_CORE_ETAG"]:format( d[4] ) );
     elseif ( d[1] == "Bug" ) then 
          if ( TitanClanDebug.State ) then
               local fError = ( "[%s] %s" ):format( TitanClan_ColorUtils( "cText", "orange", d[2] ), d[4] );
               DEFAULT_CHAT_FRAME:AddMessage( LB["TITAN_CLAN_CORE_DTAG"]:format( fError ) );
               if ( TitanClanDebug.LogEvent ) then tinsert( TitanClanDebug.Log, d[4] ); end
          end
     else
          DEFAULT_CHAT_FRAME:AddMessage( LB["TITAN_CLAN_DEBUG_CMDE"]:format( d[1], d[2], d[3] ) );
     end
end

function testTime()
     local sec = GetTime();
     local min = sec / 60;
     local hur = min / 60;
     local day = hur / 12;
     print( ( "your computer has been runing for %s days" ):format( floor( day ) ) );
end
