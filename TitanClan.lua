-- **************************************************************************
--   TitanClan.lua
--  
--   By: KanadiaN
--        (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanClan", true );

-- **************************************************************************
-- NAME : TitanClan_Init( state )
-- DESC : For first time addon use
-- **************************************************************************
function TitanClan_Init( state )
     if ( state == "checkDB" ) then
          checkDB = true;
          TitanClanInit = false;
     elseif ( state == "init" ) then
          checkDB = false;
     end
     if ( type( TitanClan ) ~= "table" ) then TitanClan = {}; end
     if ( not TitanClanInit or TitanClanInit == nil ) then
          if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 24, LB["TITAN_CLAN_MENU_CHECKDB"] ); end
          if ( not TitanClanStaticArrays or TitanClanStaticArrays == nil ) then
               if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 26, LB["TITAN_CLAN_ARRAY"]:format( "TitanClanStaticArrays" ) ); end
               TitanClanStaticArrays = {};
          end
          if ( not TitanClanLocals or TitanClanLocals == nil ) then
               if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 30, LB["TITAN_CLAN_ARRAY"]:format( "TitanClanLocals" ) ); end
               TitanClanLocals = {};
          end
          if ( not TitanClanStaticArrays.presets or TitanClanStaticArrays.presets == nil ) then
               if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 34, LB["TITAN_CLAN_STATICARRAY_FIXED"]:format( "presets" ) ); end
               TitanClanStaticArrays.presets = { LB["TITAN_CLAN_CONFIG_NONE"], "!s (!l) !p ~ !z !c !r", "!s (!l) !uc ~ !z !r", };
          end
          if ( not TitanClanStaticArrays.sortTypes or TitanClanStaticArrays.sortTypes == nil ) then
               if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 38, LB["TITAN_CLAN_STATICARRAY_FIXED"]:format( "sortTypes" ) ); end
               TitanClanStaticArrays.sortTypes = { LB["TITAN_CLAN_CONFIG_NAME"], LB["TITAN_CLAN_CONFIG_ZONE"], LB["TITAN_CLAN_CONFIG_RANK"], LB["TITAN_CLAN_CONFIG_LEVEL"], LB["TITAN_CLAN_CONFIG_CLASS"], };
          end
          if ( not TitanClanStaticArrays.sortMethod or TitanClanStaticArrays.sortMethod == nil ) then
               if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 42, LB["TITAN_CLAN_STATICARRAY_FIXED"]:format( "sortMethod" ) ); end
               TitanClanStaticArrays.sortMethod = { LB["TITAN_CLAN_CONFIG_ASCENDING"], LB["TITAN_CLAN_CONFIG_DESCENDING"], };
          end
          if ( not TitanClanLocals.LocalClass or TitanClanLocals.LocalClass == nil ) then
               if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 46, LB["TITAN_CLAN_LOCALSARRAY_FIXED"]:format( "LocalClass" ) ); end
               TitanClanLocals.LocalClass = {};
          end
          if ( not TitanClanLocals.ColorClass or TitanClanLocals.ColorClass == nil ) then
               if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 50, LB["TITAN_CLAN_LOCALSARRAY_FIXED"]:format( "ColorClass" ) ); end
               TitanClanLocals.ColorClass = {};
          end
          if ( not TitanClanLocals.ToEnglish or TitanClanLocals.ToEnglish == nil ) then
               if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 54, LB["TITAN_CLAN_LOCALSARRAY_FIXED"]:format( "ToEnglish" ) ); end
               TitanClanLocals.ToEnglish = {};
          end
          if ( not TitanClanDebug or TitanClanDebug == nil ) then
               if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 62, LB["TITAN_CLAN_ARRAY"]:format( "TitanClanDebug" ) ); end
               TitanClanDebug = {
                    State = false,
                    Event = false,
                    LogEvent = false,
                    dLine = "",
                    FormatToRemove = LB["TITAN_CLAN_CONFIG_NONE"], 
                    Temp = {},
                    Log = {},
               };
          end
          if ( not TitanClan.Settings or TitanClan.Settings == nil ) then
               if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 74, LB["TITAN_CLAN_ARRAY_FIXED"]:format( "Settings" ) ); end
               TitanClan.Settings = {
                    ShowMem = true,
                    DisplayB = true,
                    tFormat = "!s (!l) !uc ~ !z !r",
                    sFormat = ( "<%s>" ):format( LB["TITAN_CLAN_CONFIG_AWAY"] ),
                    dFormat = ( "<%s>" ):format( LB["TITAN_CLAN_CONFIG_DND"] ),
                    sType = LB["TITAN_CLAN_CONFIG_NAME"],
                    sMethod = LB["TITAN_CLAN_CONFIG_ASCENDING"],
                    maxTooltip = 30,
                    Version = LB["TITAN_CLAN_CORE_VERSION"],
                    ArrayVersion = LB["TITAN_CLAN_CORE_AVER"],
                    iCount = 0,
                    oldTime = 0,
                    aStyle = "guild",
                    Ambiguate = false,
               };
          end
          if ( not TitanClan.AddAuth or TitanClan.AddAuth == nil ) then
               if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 89, LB["TITAN_CLAN_ARRAY_FIXED"]:format( "AddAuth" ) ); end
               TitanClan.AddAuth = {};
          end
          TitanClanInit = true;
          if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 93, LB["TITAN_CLAN_MENU_CHECKDB_FINSIHED"] ); end
     end
     if ( not TitanClanGuild or TitanClanGuild ) then
          if ( checkDB ) then TitanClan_DebugUtils( "Info", "TitanClan", 58, LB["TITAN_CLAN_ARRAY"]:format( "TitanClanGuild" ) ); end
          TitanClanGuild = {};
         TitanClan_GuildUtils( "gSetup" );
     end
     if ( type( TitanClanProfile ) ~= "table" ) then
          TitanClanProfile = {};
     end
     TitanClanProfile = {
          Player = LB["TITAN_CLAN_CONFIG_UNKNOWN"],
          Faction = LB["TITAN_CLAN_CONFIG_UNKNOWN"],
          Realm = LB["TITAN_CLAN_CONFIG_UNKNOWN"],
          Level = LB["TITAN_CLAN_CONFIG_UNKNOWN"],
          Class = LB["TITAN_CLAN_CONFIG_UNKNOWN"],
     };
     TitanClan.Settings.iCount = 0;
     TitanClan_CoreUtils( "getLocals" );
     TitanClanDebug.FormatToRemove = LB["TITAN_CLAN_CONFIG_NONE"];
end

-- **************************************************************************
-- NAME : TitanPanelClanButton_OnEvent()
-- DESC : Event Handler
-- **************************************************************************
function TitanPanelClanButton_OnEvent( self, event, ... )
     if ( event == "ADDON_LOADED" and ... == "TitanClan" ) then
          TitanClan_Init( "init" );
     elseif ( event == "PLAYER_ENTERING_WORLD" ) then
          if ( TitanClan.Settings.DisplayB ) then TitanClan_CoreUtils( "Banner" ); end
          TitanClan.Settings.oldTime = 0;
          TitanClan_SetUser();
     elseif ( event == "GUILD_RANKS_UPDATE" ) then
          TitanClan_GuildUtils( "buildRanks" )
     elseif ( event == "GUILD_ROSTER_UPDATE" ) then
          C_GuildInfo.GuildRoster();
          TitanClan_GuildUtils( "buildGuild" );
     end
     -- if ( TitanClan.Settings.iCount < 6 ) then
          TitanClan_CoreUtils( "setIcon" );
     -- end
     TitanPanelButton_UpdateButton( LB["TITAN_CLAN_CORE_ID"] );
     TitanPanelButton_UpdateTooltip( self );

end

-- **************************************************************************
-- NAME : TitanPanelClanButton_GetButtonText()
-- DESC : Build Button Text
-- **************************************************************************
function TitanPanelClanButton_GetButtonText()
     local buttonRichText, buttonRichLabel, numT, numO, RnumT, RnumO;
     if ( IsInGuild() ) then
          numT, numO = GetNumGuildMembers();
          RnumT = TitanClan_ColorUtils( "cText", "white", numT );
          RnumO = TitanClan_ColorUtils( "cText", "green", numO );
          buttonRichLabel = LB["TITAN_CLAN_BUTTON_LABEL"]:format( GetGuildInfo( "player" ) );
          buttonRichText = LB["TITAN_CLAN_BUTTON_TEXT"]:format( RnumO, RnumT );
     else
          buttonRichLabel = "";
          buttonRichText = LB["TITAN_CLAN_BUTTON_CLANLESS"];
     end
     return buttonRichLabel, buttonRichText;
end

-- **************************************************************************
-- NAME : TitanPanelClanButton_OnClick()
-- DESC : Toggles Friends Dialog
-- **************************************************************************
function TitanPanelClanButton_OnClick( self, button )
     if ( button == "LeftButton" ) then
          if ( IsAltKeyDown() and TitanClanDebug.State ) then
               TitanClanInit = nil;
               ReloadUI();
          else
               ToggleGuildFrame();
          end
     else
          TitanPanelButton_OnClick( self, button );
     end
end

-- **************************************************************************
-- NAME : TitanClan_SetUser()
-- DESC : Sets up the User for use with the data array
-- **************************************************************************
function TitanClan_SetUser()
     local Faction = UnitFactionGroup( "player" );
     local Realm = GetRealmName();
     local Player = GetUnitName( "player" );
     local Level = UnitLevel( "player" );
     local Class = UnitClass( "player" );
     if ( not TitanClanProfile.Player or TitanClanProfile.Player ~= Player ) then TitanClanProfile.Player = Player; end
     if ( not TitanClanProfile.Faction or TitanClanProfile.Faction ~= Faction ) then TitanClanProfile.Faction = Faction; end
     if ( not TitanClanProfile.Level or TitanClanProfile.Level ~= Level ) then TitanClanProfile.Level = tonumber( Level ); end
     if ( not TitanClanProfile.Class or TitanClanProfile.Class ~= Class ) then TitanClanProfile.Class = Class; end
     if ( not TitanClanProfile.Realm or TitanClanProfile.Realm ~= Realm ) then TitanClanProfile.Realm = Realm; end
     if ( IsInGuild() ) then
          C_GuildInfo.GuildRoster();
     end
end

-- **************************************************************************
-- NAME : TitanClan_UpdateRoster()
-- DESC : Sets up data array of the guild
-- **************************************************************************
function TitanClan_UpdateRoster()
     TitanClan_GuildUtils( "buildRanks" )
     TitanClan_GuildUtils( "buildGuild" );
end
