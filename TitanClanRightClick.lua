-- **************************************************************************
--   TitanClanRightClick.lua
--
--   By: KanadiaN
--        (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanClan", true );

-- **************************************************************************
-- NAME : TitanPanelRightClickMenu_PrepareClanMenu()
-- DESC : Builds Right Click Interactive Menu
-- **************************************************************************
function TitanPanelRightClickMenu_PrepareClanMenu()
     local name, rank, index, online, coloredValue, coloredName, curMenu, charTable
     local aKey = TitanClanProfile.aKey
     TitanClan_GuildUtils( "gSort" );
     if ( TitanPanelRightClickMenu_GetDropdownLevel() == 2 ) then
          table.foreach( TitanClanGuild.Ranks, function( Key1, Value1 )
                    coloredValue = TitanClan_ColorUtils( "cRank", Key1 - 1, Value1, TitanClanGuild.numRanks );
	            if ( TitanPanelRightClickMenu_GetDropdMenuValue() == coloredValue ) then
                         --TitanPanelRightClickMenu_AddTitle( coloredValue, TitanPanelRightClickMenu_GetDropdownLevel() );
                         table.foreach( TitanClanGuild.Stream, function( Key2, Value2 )
                                   charTable = TitanClanGuild.Stream[Key2];
                                   if ( charTable.Rank == Value1 ) then
                                        info = {};
                                        info.notCheckable = true;
                                        info.text = TitanClan_ColorUtils( "cClass", charTable.Class, charTable.Name );
                                        info.hasArrow = 1;
                                        TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                                   end
                              end
                         );
                    end
               end
          );
          return;
     end
     if ( TitanPanelRightClickMenu_GetDropdownLevel() == 3 ) then
          table.foreach( TitanClanGuild.Stream, function( Key1, Value1 )
                    charTable = TitanClanGuild.Stream[Key1];
                    coloredName = TitanClan_ColorUtils( "cClass", charTable.Class, charTable.Name );
                    if ( TitanPanelRightClickMenu_GetDropdMenuValue() == coloredName ) then
                         -- TitanPanelRightClickMenu_AddTitle( coloredName, TitanPanelRightClickMenu_GetDropdownLevel() );
                         -- whisper
                         info = {};
                         info.notCheckable = true;
                         info.text = LB["TITAN_CLAN_MENU_WHISPER"]:format( coloredName );
                         info.func = TitanClan_ClanWhisper;
                         info.value = charTable.Name;
                         TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );			
                         -- invite
                         info = {};
                         info.notCheckable = true;
                         info.text = LB["TITAN_CLAN_MENU_INVITE"]:format( coloredName );
                         info.func = TitanClan_ClanGroupInvite;
                         info.value = charTable.Name;
                         TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                         -- who
                         info = {};
                         info.notCheckable = true;
                         info.text = LB["TITAN_CLAN_MENU_WHO"]:format( coloredName );
                         info.func = TitanClan_SendClanWho;
                         info.value = charTable.Name;
                         TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                         -- friend
                         info = {};
                         info.notCheckable = true;
                         info.text = LB["TITAN_CLAN_MENU_ADDFRIEND"]:format( coloredName );
                         info.func = TitanClan_ClanToFriend;
                         info.value = charTable.Name;
                         TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
                    end
               end
          );
          return;
     end
     TitanPanelRightClickMenu_AddTitle( TitanPlugins[ LB["TITAN_CLAN_CORE_ID"] ].menuText );
     curMenu = {};
     if ( TitanClanGuild.TotalOnline > 0 ) then
          table.foreach( TitanClanGuild.Ranks, function( Key3, Value3 )
                    table.foreach( TitanClanGuild.Stream, function( Key4, Value4 )
                              charTable = TitanClanGuild.Stream[Key4];
                              if ( charTable.Rank == Value3 and not curMenu[Value3] ) then
                                   rankIndex = Key3 - 1;
                                   info = {};
                                   info.notCheckable = true;
                                   info.text = TitanClan_ColorUtils( "cRank", rankIndex, Value3, TitanClanGuild.numRanks );
                                   info.hasArrow = 1;
                                   TitanPanelRightClickMenu_AddButton( info );
                                   curMenu[Value3] = true;
                              end
                         end
                    );
               end
          );
     end
     TitanPanelRightClickMenu_AddSpacer();
     curMenu = nil;
     info = {};
     info.notCheckable = true;
     info.text = LB["TITAN_CLAN_MENU_SETTINGS"];
     info.value = LB["TITAN_CLAN_MENU_SETTINGS"];
     info.func = function() collectgarbage( "collect" ); TitanClan_InitGUI(); end
     TitanPanelRightClickMenu_AddButton( info, TitanPanelRightClickMenu_GetDropdownLevel() );
     TitanPanelRightClickMenu_AddSpacer();
     -- TitanPanelRightClickMenu_AddToggleIcon( LB["TITAN_CLAN_CORE_ID"] );
     -- TitanPanelRightClickMenu_AddToggleLabelText( LB["TITAN_CLAN_CORE_ID"] );
     TitanPanelRightClickMenu_AddControlVars( LB["TITAN_CLAN_CORE_ID"] );
     -- TitanPanelRightClickMenu_AddSpacer();
     -- TitanPanelRightClickMenu_AddCommand( LB["TITAN_CLAN_MENU_HIDE"], LB["TITAN_CLAN_CORE_ID"], TITAN_PANEL_MENU_FUNC_HIDE );
     TitanPanelRightClickMenu_AddSpacer();
     TitanPanelRightClickMenu_AddCommand( LB["TITAN_CLAN_MENU_CLOSE"], LB["TITAN_CLAN_CORE_ID"], function() return; end );
     guildTable = nil;
end

-- **************************************************************************
-- NAME : TitanClan_ClanWhisper( self )
-- DESC : Opens a wisper with the selected person
-- **************************************************************************
function TitanClan_ClanWhisper( self )
     if ( not ChatFrame1EditBox:IsVisible() ) then
          ChatFrame_OpenChat( ( "/w %s" ):format( self.value ) );
     else
          ChatFrame1EditBox:SetText( ( "/w %s" ):format( self.value ) );
     end
end

-- **************************************************************************
-- NAME : TitanClan_ClanGroupInvite( self )
-- DESC : Invites clan member to group
-- **************************************************************************
function TitanClan_ClanGroupInvite( self )
     C_PartyInfo.InviteUnit( self.value );
end

-- **************************************************************************
-- NAME : TitanClan_SendClanWho( self )
-- DESC : Does o who on the clan member
-- **************************************************************************
function TitanClan_SendClanWho( self )
     C_FriendList.SendWho( self.value )
end

-- **************************************************************************
-- NAME : TitanClan_ClanToFriend()
-- DESC : Adds Clan member to friends list
-- **************************************************************************
function TitanClan_ClanToFriend( self )
     C_FriendList.AddFriend( self.value )
end
