-- **************************************************************************
--   TitanClanConfig.lua
--
--   By: KanadiaN
--        (Lindarena@Laughing Skull)
-- **************************************************************************

local L = LibStub( "AceLocale-3.0" ):GetLocale( "Titan", true );
local LB = LibStub( "AceLocale-3.0" ):GetLocale( "TitanClan", true );
local Config = LibStub( "AceConfig-3.0" );
local Dialog = LibStub( "AceConfigDialog-3.0" );

-- **************************************************************************
-- NAME : Core Vars
-- DESC : These are here so they do not get changed like the locales would
-- **************************************************************************

local PlugInName = "TitanClan";
LB["TITAN_CLAN_CORE_ID"] = "Clan";
LB["TITAN_CLAN_CORE_VERSION"] = GetAddOnMetadata( PlugInName, "Version" );
LB["TITAN_CLAN_CORE_DTAG"] = "|cffff0000<TitanClan Debug>|r %s";
LB["TITAN_CLAN_CORE_ITAG"] = "|cffff9900<TitanClan>|r %s";
LB["TITAN_CLAN_CORE_ETAG"] = "|cffff9900<TitanClan Error>|r %s";
LB["TITAN_CLAN_CORE_AVER"] = tonumber( 1 );

-- **************************************************************************
-- NAME : TitanClan_InitGUI();
-- DESC : Open Config Dialog
-- **************************************************************************
function TitanClan_InitGUI( )
     Dialog:Open( PlugInName );
end

-- **************************************************************************
-- NAME : TitanClan_ConfigUtils( a )
-- DESC : Pull information from .toc for config dialog
-- **************************************************************************
local function TitanClan_ConfigUtils( a )
     if ( a == "GetAuthor" ) then
          return GetAddOnMetadata( PlugInName, "Authors" );
     elseif ( a == "GetTranslation" ) then
          return GetAddOnMetadata( PlugInName, "X-Translation" );
     elseif ( a == "GetCategory" ) then
          return GetAddOnMetadata( PlugInName, "X-Category" );
     elseif ( a == "GetEmail" ) then
          return GetAddOnMetadata( PlugInName, "X-Email" );
     elseif ( a == "GetWebsite" ) then
          return GetAddOnMetadata( PlugInName, "X-Website" );
     elseif ( a == "GetVersion" ) then
          return tostring( GetAddOnMetadata( PlugInName, "Version" ) );
     end
end

-- **************************************************************************
-- NAME : ClanOpt
-- DESC : this is the root of the config dialog
-- **************************************************************************
local ClanOpt = {
     type = "group",
     name = LB["TITAN_CLAN_ADDON_LABEL"],
     args = {
          confgendesc = {
               type = "description", order = 1,
               name = LB["TITAN_CLAN_ABOUT_DESC"],
               cmdHidden = true
          },
	  cat1 = {
               type = "group", order = 3,
               name = LB["TITAN_CLAN_ABOUT"],
               args = {
                    confversiondesc = {
                         type = "description", order = 1,
                         name = LB["TITAN_CLAN_ABOUT_VERSION"]:format( TitanClan_ConfigUtils( "GetVersion" ) ),
                         cmdHidden = true
                    },
                    confauthordesc = {
                         type = "description", order = 2,
                         name = LB["TITAN_CLAN_ABOUT_AUTHOR"]:format( TitanClan_ConfigUtils( "GetAuthor" ) ),
                         cmdHidden = true
                    },
                    conflangauthordesc = {
                         type = "description", order = 3,
                         name = LB["TITAN_CLAN_ABOUT_TRANSLATION"]:format( TitanClan_ConfigUtils( "GetTranslation" ) ),
                         cmdHidden = true
                    },
                    confcatdesc = {
                         type = "description", order = 4,
                         name = LB["TITAN_CLAN_ABOUT_CATEGORY"]:format( TitanClan_ConfigUtils( "GetCategory" ) ),
                         cmdHidden = true
                    },
                    confemaildesc = {
                         type = "description", order = 5,
                         name = LB["TITAN_CLAN_ABOUT_EMAIL"]:format( TitanClan_ConfigUtils( "GetEmail" ) ),
                         cmdHidden = true
                    },
                    confwebsitedesc = {
                         type = "description", order = 6,
                         name = LB["TITAN_CLAN_ABOUT_WEBSITE"]:format( TitanClan_ConfigUtils( "GetWebsite" ) ),
                         cmdHidden = true
                    },
                    confmemorydesc = {
                         type = "description", order = 7,
                         name = function()
                              UpdateAddOnMemoryUsage();
                              mem = floor( GetAddOnMemoryUsage( PlugInName ) );
                              return LB["TITAN_CLAN_ABOUT_MEMORY"]:format( mem );
                         end,
                         cmdHidden = true,
                    },
               },
	  },
          cat2 = {
               type = "group", order = 4,
	       name = LB["TITAN_CLAN_CONFIG_HEADER_SETTINGS"],
               args = {
                    showbannor = {
                         type = "toggle", order = 1,
                         name = LB["TITAN_CLAN_CONFIG_BANNER"],
                         desc = LB["TITAN_CLAN_CONFIG_BANNER_DESC"],
                         get = function() return TitanClan.Settings.DisplayB end,
                         set = function( _, value ) TitanClan.Settings.DisplayB = value; end,
                    },
                    showmem = {
                         type = "toggle", order = 2,
                         name = LB["TITAN_CLAN_CONFIG_SHOWMEM"],
                         desc = LB["TITAN_CLAN_CONFIG_SHOWMEM_DESC"]:format( LB["TITAN_CLAN_ADDON_LABEL"] ),
                         get = function() return TitanClan.Settings.ShowMem end,
                         set = function( _, value ) TitanClan.Settings.ShowMem = value; end,
                    },
                    debugmode = {
                         type = "toggle", order = 3,
                         name = LB["TITAN_CLAN_CORE_DEBUG"],
                         desc = LB["TITAN_CLAN_CORE_DEBUG_DESC"],
                         hidden = function() return not TitanClan_CoreUtils( "Author", ( "%s@%s" ):format( TitanClanProfile.Player, TitanClanProfile.Realm ) ) end,
                         get = function() return TitanClanDebug.State; end,
                         set = function( _, value ) TitanClanDebug.State = value; end,
                    },
               },
          },
	  cat3 = {
               type = "group", order = 5,
               name = LB["TITAN_CLAN_CONFIG_HEADER_TOOLTIP"],
               args = {
	            tooltippresets = {
                         type = "select", order = 1, width = "full",
                         name = LB["TITAN_CLAN_CONFIG_PRESETS"],
                         desc = LB["TITAN_CLAN_CONFIG_PRESETS_DESC"],
                         get = function() return TitanClan.Settings.tFormat; end,
                         set = function( _, value ) TitanClan.Settings.tFormat = value; end,
                         values = function()
                              local preList = {};
                              table.foreach( TitanClanStaticArrays.presets, function( key, value )
                                        if ( TitanClan.Settings.tFormat == value ) then
                                             preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                        else
                                             preList[value] = value;
                                        end
			           end
                              )
                              return preList;
                         end
                    },
	            sorttypes = {
                         type = "select", order = 3,
                         name = LB["TITAN_CLAN_CONFIG_SORTTYPES"],
                         desc = LB["TITAN_CLAN_CONFIG_SORTTYPES_DESC"],
                         get = function() return TitanClan.Settings.sType; end,
                         set = function( _, value )
                              TitanClan.Settings.sType = value;
                         end,
                         values = function()
                              local preList = {};
                              table.foreach( TitanClanStaticArrays.sortTypes, function( key, value )
                                        if ( TitanClan.Settings.sType == value ) then
                                             preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                        else
                                             preList[value] = value;
                                        end
                                   end
                              )
                              return preList;
                         end
                    },
	            sortmethod = {
                         type = "select", order = 4,
                         name = LB["TITAN_CLAN_CONFIG_SORTMETHOD"],
                         desc = LB["TITAN_CLAN_CONFIG_SORTMETHOD_DESC"],
                         get = function() return TitanClan.Settings.sMethod; end,
                         set = function( _, value )
                              TitanClan.Settings.sMethod = value;
                         end,
                         values = function()
                              local preList = {};
                              table.foreach( TitanClanStaticArrays.sortMethod, function( key, value )
                                        if ( TitanClan.Settings.sMethod == value ) then
                                             preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                        else
                                             preList[value] = value;
                                        end
                                   end
                              )
                              return preList;
                         end
                    },
                    maxtooltip = {
                         type = "range", order = 5,
                         name = LB["TITAN_CLAN_CONFIG_MAXTOOLTIP"],
                         desc = LB["TITAN_CLAN_CONFIG_MAXTOOLTIP_DESC"],
                         min = 2,
                         max = 50,
                         step = 1,
                         get = function() return TitanClan.Settings.maxTooltip; end,
                         set = function( _, value ) TitanClan.Settings.maxTooltip = value; end,
                    },
               },
	  },
	  cat4 = {
               type = "group", order = 6,
               name = LB["TITAN_CLAN_CONFIG_HEADER_ADD"],
               args = {
                    tooltipaddcustom = {
			 type = "input", order = 1, width = "full",
			 name = LB["TITAN_CLAN_CONFIG_CUSTOM_TOOLTIP"],
			 desc = LB["TITAN_CLAN_CONFIG_CUSTOM_TOOLTIP_DESC"],
			 get = function() return end,
			 set = function( _, value ) table.insert( TitanClanStaticArrays.presets, value ); end,
		    },
                    nulloptionct = {
			 type = "description", order = 2,
			 name = LB["TITAN_CLAN_CONFIG_CUSTOM_TOOLTIP_INST"],
			 cmdHidden = true;
                    },
               },
	  },
          cat5 = {
               type = "group", order = 7,
               name = LB["TITAN_CLAN_CONFIG_HEADER_REMOVE"],
	       args = {
	            tooltippresetsremove = {
                         type = "select", order = 1, width = "full",
                         name = LB["TITAN_CLAN_CONFIG_PRESETS"],
                         desc = LB["TITAN_CLAN_CONFIG_REMOVE_PRESETS_DESC"],
                         get = function() return TitanClanDebug.FormatToRemove; end,
                         set = function( _, value ) 
                              TitanClanDebug.FormatToRemove = value;
			 end,
                         values = function()
                              local preList = {};
                              table.foreach( TitanClanStaticArrays.presets, function( key, value )
                                        if ( value ~= TitanClanDebug.FormatToRemove ) then
                                             preList[value] = ( "|cffffff9a%s|r" ):format( value );
                                        else
                                             preList[value] = value;
                                        end
                                   end
                              )
                              return preList;
                         end
                    },
		    tooltippresetremovebutton = {
                         type = "execute", order = 2,
			 name = LB["TITAN_CLAN_CONFIG_REMOVE"],
			 desc = LB["TITAN_CLAN_CONFIG_REMOVE_DESC"],
                         disabled = function()
                              if ( TitanClanDebug.FormatToRemove ~= LB["TITAN_CLAN_CONFIG_NONE"] ) then
                                   return false;
                              else
                                   return true;
                              end
                         end,
                         func = function()
                              if ( TitanClanDebug.FormatToRemove == LB["TITAN_CLAN_CONFIG_NONE"] ) then return; end
                              local key, value
			      local found = "NotSet";
                              table.foreach( TitanClanStaticArrays.presets, function( key, value )
                                        if ( TitanClan.Settings.tFormat == TitanClanDebug.FormatToRemove and value ~= TitanClanDebug.FormatToRemove and key ~= 1 and found == "NotSet" ) then 
                                             TitanClan.Settings.tFormat = value;
                                             found = "Set";
                                        end
                                        if ( value == TitanClanDebug.FormatToRemove ) then
                                             table.remove( TitanClanStaticArrays.presets, key );
                                             TitanClanDebug.FormatToRemove = LB["TITAN_CLAN_CONFIG_NONE"];
                                        end
                                   end
                              )
                         end,
		    },
               },
          },
          cat6 = {
               type = "group", order = 8,
               name = LB["TITAN_CLAN_CORE_DEBUG_HEADER"],
               hidden = function() return not TitanClanDebug.State; end,
               args = {
                    event = {
                         type = "toggle", order = 1,
                         name = LB["TITAN_CLAN_CORE_DEBUG_EVENT"],
                         desc = LB["TITAN_CLAN_CORE_DEBUG_EVENT_DESC"],
                         get = function() return TitanClanDebug.Event; end,
                         set = function( _, value ) TitanClanDebug.Event = value; end,
                    },
                    logevent = {
                         type = "toggle", order = 2,
                         name = LB["TITAN_CLAN_CORE_DEBUG_LOGEVENT"],
                         desc = LB["TITAN_CLAN_CORE_DEBUG_LOGEVENT_DESC"],
                         get = function() return TitanClanDebug.LogEvent; end,
                         set = function( _, value ) TitanClanDebug.LogEvent = value; end,
                    },
                    remauth = {
                         type = "execute", order = 5,
                         name = LB["TITAN_CLAN_CORE_DEBUG_REMAUTH"],
                         hidden = function()
                              if ( getn( TitanClan.AddAuth ) == 0 ) then
                                   return true;
                              else
                                   return false;
                              end
                         end,
                         desc = function()
                              local author = ( "%s@%s" ):format( TitanClanProfile.Player, TitanClanProfile.Realm )
                              return LB["TITAN_CLAN_CORE_DEBUG_REMAUTH_DESC"]:format( author )
                         end,
                         func = function()
                              TitanClan_CoreUtils( "RemAuth" );
                              TitanClanDebug.State = false;
                              TitanClanDebug.Event = false;
                              TitanClanDebug.LogEvent = false;
                         end,
                    },
               },
          },
     },
}

-- **************************************************************************
-- NAME : TitanPanelClanButton_OnLoad( self )
-- DESC : Registers the plugin upon it loading
-- **************************************************************************
function TitanPanelClanButton_OnLoad( self )
     self.registry = {
          id = LB["TITAN_CLAN_CORE_ID"],
          builtIn = false,
          version = TITAN_VERSION,
          menuText = LB["TITAN_CLAN_MENU_TEXT"],
          buttonTextFunction = "TitanPanelClanButton_GetButtonText",
	  tooltipTitle = LB["TITAN_CLAN_TOOLTIP"],
          tooltipCustomFunction = TitanPanelClan_SetTooltip,
          iconWidth = 16,
          category = "Information",
          controlVariables = {
               ShowIcon = true,
               ShowLabelText = true,
          },
          savedVariables = {
	       ShowIcon = 1,
	       ShowLabelText = 1,
          }
     };

     self:RegisterEvent( "PLAYER_ENTERING_WORLD" );
     self:RegisterEvent( "PLAYER_LEAVING_WORLD" );
     self:RegisterEvent( "PLAYER_GUILD_UPDATE" );
     self:RegisterEvent( "GUILD_ROSTER_UPDATE" );
     self:RegisterEvent( "GUILD_RANKS_UPDATE" );
     self:RegisterEvent( "ADDON_LOADED" );

     -- Titan Clan Config
     Config:RegisterOptionsTable( PlugInName, ClanOpt );
end

-- **************************************************************************
-- NAME : SLASH_TITANCLAN*
-- DESC : Slash command setup
-- **************************************************************************
SLASH_TITANCLAN1 = "/titanclan";
SLASH_TITANCLAN2 = "/tc";
SlashCmdList["TITANCLAN"] = function( msg )
     TitanClan_CommandHandler( "titanclan", msg );
end

-- **************************************************************************
-- NAME : TitanClan_ShowHelp( cmd )
-- DESC : Slash command help display
-- **************************************************************************
function TitanClan_ShowHelp( cmd )
     TitanClan_DebugUtils( "Info", "TitanClanConfig", 376, LB["TITAN_CLAN_HELP_LINE1"] );
     TitanClan_DebugUtils( "Info", "TitanClanConfig", 377, LB["TITAN_CLAN_HELP_LINE2"]:format( cmd ) );
     TitanClan_DebugUtils( "Info", "TitanClanConfig", 378, LB["TITAN_CLAN_HELP_LINE3"]:format( cmd ) );
     TitanClan_DebugUtils( "Info", "TitanClanConfig", 379, LB["TITAN_CLAN_HELP_LINE4"]:format( cmd ) );
     TitanClan_DebugUtils( "Info", "TitanClanConfig", 380, LB["TITAN_CLAN_HELP_LINE5"]:format( cmd ) );
     local auth = TitanClan_CoreUtils( "Author", ( "%s@%s" ):format( TitanClanProfile.Player, TitanClanProfile.Realm ) );
     if ( auth ) then
          TitanClan_DebugUtils( "Info", "TitanClanConfig", 383, LB["TITAN_CLAN_HELP_ADDAUTH"]:format( cmd ) );
          TitanClan_DebugUtils( "Info", "TitanClanConfig", 384, LB["TITAN_CLAN_HELP_REMAUTH"]:format( cmd ) );
     end
end

-- **************************************************************************
-- NAME : TitanClan_CommandHandler()
-- DESC : Slash command handler
-- **************************************************************************
function TitanClan_CommandHandler( slash, command )
     local cmd1, arg1, arg2
     _, _, cmd1, arg1, arg2 = strfind( strlower( string.lower( command ) ), "(%w+)[ ]?(%w*)[ ]?([-%w]*)" );
     if ( not cmd1 ) then cmd1 = string.lower( command ); end
     if ( not arg1 ) then arg1 = ""; end
     if ( not arg2 ) then arg2 = ""; end
     if ( cmd1 == "" or cmd1 == "help" ) then
          TitanTreasury_ShowHelp( slash );
     elseif ( cmd1 == "options" or cmd1 == "config" ) then
          TitanClan_InitGUI();
     elseif ( cmd1 == "reset" ) then
          if ( arg1 == "all" or arg1 == "settings" ) then rLoad = true;
          else rLoad = false; end
          TitanClan_ResetUtils( rLoad, cmd1, arg1, arg2 );
     elseif ( cmd1 == "checkdb" ) then
          TitanClan_ResetUtils( false, cmd1 );
     elseif ( cmd1 == "addauth" ) then
          TitanClan_CoreUtils( cmd1 );
     elseif ( cmd1 == "remauth" ) then
          TitanClan_CoreUtils( cmd1 );
     else
          TitanClan_DebugUtils( "Error", "TitanClanConfig", 417, LB["TITAN_CLAN_HELP_ERROR"]:format( cmd1 ) )
     end
end
